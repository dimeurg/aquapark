#pragma once
#include <QDate>

struct ScheduleDto
{
    ScheduleDto(const QDate dateStart = QDate(), const QDate dateEnd = QDate());

    QDate dateStart;
    QDate dateEnd;
};
