#pragma once
#include <QDate>
#include <set>
#include "Account/BaseAccount/AccountInfoDto.h"
#include "HallDto.h"

struct ClassDto
{
    ClassDto(const HallDto& hall, const AccountInfoDto& coach);

    std::set<AccountInfoDto> participants;
    AccountInfoDto coach;
    HallDto hall;
    QDate data;
};
