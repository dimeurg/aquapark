#pragma once
#include "ScheduleDto.h"
#include <QString>

struct HallDto
{
    HallDto(const QString& hall);

    ScheduleDto schedule;
    QString name;
};
