#pragma once

//stl
#include <set>

//Qt
#include "QJsonArray"
#include "QJsonObject"
#include "QJsonDocument"

//Custom
#include "SeasonTicket/SeasonTicketDto.h"
#include "BaseAccount/AccountInfoDto.h"

const QString SeasonTickets = "seasonTickets";
const QString Customer = "customer";

class JsonParser
{
public:
    static SeasonTicketDto parseOneSeasonTicketJson(const QJsonObject& jsonTicket);
    static SeasonTicketDto parseOneSeasonTicketJson(const QString &jsonText);
    static std::set<SeasonTicketDto> parseSeasonTicketsJson(const QString &jsonText);
    static QString parseSeasonTicketsJsonToList(const QString &jsonText);
    static AccountInfoDto parseOneAccountInfo(const QJsonObject& jsonCustomer);
    static AccountInfoDto parseOneAccountInfo(const QString &jsonText);

    static bool seasonTicketJsonIsValid(const QJsonObject& jsonTicket);

};
