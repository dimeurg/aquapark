#include "JsonParser.h"
#include <QDebug>

SeasonTicketDto JsonParser::parseOneSeasonTicketJson(const QJsonObject &jsonTicket)
{
    QString name = jsonTicket["name"].toString();
    int duration = jsonTicket["duration"].toInt();
    int price = jsonTicket["price"].toInt();
    QString hallName = jsonTicket["hallName"].toString();
    QDate dateStart = QDate::fromString(jsonTicket["dateStart"].toString());
    QDate dateEnd = QDate::fromString(jsonTicket["dateEnd"].toString());

    return SeasonTicketDto(name, duration, price, hallName, dateStart, dateEnd);
}

SeasonTicketDto JsonParser::parseOneSeasonTicketJson(const QString &jsonText)
{
    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonText.toUtf8());
    QJsonObject jsonResult = jsonDoc.object();

    if(jsonResult.contains("seasonTicket"))
    {
        QJsonArray jsonRow = jsonResult["seasonTicket"].toArray();
        QJsonObject ticket = jsonRow[0].toObject();
        return parseOneSeasonTicketJson(ticket);
    }

    return parseOneSeasonTicketJson(jsonResult);
}

std::set<SeasonTicketDto> JsonParser::parseSeasonTicketsJson(const QString &jsonText)
{
    std::set<SeasonTicketDto> seasonTickets;

    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonText.toUtf8());
    QJsonObject jsonResult = jsonDoc.object();

    if(jsonResult.contains(SeasonTickets))
    {
        QJsonArray jsonRow = jsonResult["seasonTickets"].toArray();
        for(int i = 0; i < jsonRow.count(); ++i)
        {
            QJsonObject ticket = jsonRow[i].toObject();
            if(!seasonTicketJsonIsValid(ticket))
            {
                continue;
            }

            seasonTickets.insert(parseOneSeasonTicketJson(ticket));
        }
    }

    else
    {
        qDebug() << "cant parse json";
    }

    return seasonTickets;
}

QString JsonParser::parseSeasonTicketsJsonToList(const QString &jsonText)
{
    QString response;

    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonText.toUtf8());
    QJsonObject jsonResult = jsonDoc.object();

    if(jsonResult.contains(SeasonTickets))
    {
        QJsonArray jsonRow = jsonResult["seasonTickets"].toArray();
        for(int i = 0; i < jsonRow.count(); ++i)
        {
            QJsonObject ticket = jsonRow[i].toObject();
            if(!seasonTicketJsonIsValid(ticket))
            {
                continue;
            }
            response += "ticket #" + QString::number(i);

            response += "\nname: "      + ticket["name"].toString();
            response += "\nduration: "  + QString::number(ticket["duration"].toInt());
            response += "\nprice: "     + QString::number(ticket["price"].toInt());
            response += "\nhallName: "  + ticket["hallName"].toString();
            response += "\ndateStart: " + ticket["dateStart"].toString();
            response += "\ndateEnd: "   + ticket["dateEnd"].toString();

            response += "\n\n";
        }
    }

    else
    {
        qDebug() << "cant parse json";
    }

    return response;
}

AccountInfoDto JsonParser::parseOneAccountInfo(const QJsonObject &jsonInfo)
{
    QString name = jsonInfo["name"].toString();
    QString surname = jsonInfo["surname"].toString();
    QString email = jsonInfo["email"].toString();
    QString phone = jsonInfo["phone"].toString();

    return AccountInfoDto(name, surname, email, phone);
}

AccountInfoDto JsonParser::parseOneAccountInfo(const QString &jsonText)
{
    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonText.toUtf8());
    QJsonObject jsonResult = jsonDoc.object();

    if(jsonResult.contains("accountInfo"))
    {
        return parseOneAccountInfo(jsonResult["accountInfo"].toObject());
    }

    else
    {
        qDebug() << "customer not found";
        return AccountInfoDto("", "", "", "");
    }
}

bool JsonParser::seasonTicketJsonIsValid(const QJsonObject& jsonTicket)
{
    //todo
    return true;
}

