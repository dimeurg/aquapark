#include "DBManagerBase.h"
#include <QSqlDatabase>

//Qt
#include <QDebug>

DBManager::DBManager(const QString &driver, const QString &host, int port, const QString &dbName, const QString &userName, const QString &password)
    :m_database(QSqlDatabase::addDatabase(driver))
{
    m_database.setHostName(host);
    m_database.setPort(port);
    m_database.setDatabaseName(dbName);
    m_database.setUserName(userName);
    m_database.setPassword(password);

    if(!m_database.open())
    {
        qDebug() << "cant open database";
    }
}

QSqlQuery DBManager::executeQuery(const QString &queryText, const QVariantList &arguments)
{
    QSqlQuery query(m_database);
    query.prepare(queryText.toUtf8());

    for(int i = 0; i < arguments.count(); ++i)
    {
        query.bindValue(i, arguments[i].toString().toUtf8());
    }

    query.exec();
    return query;
}

QSqlQuery DBManager::requestHandle(const QString& query, const QVariantList &arguments)
{
     QSqlQuery response = executeQuery(query, arguments);
     return response;
}

