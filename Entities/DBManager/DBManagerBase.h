#pragma once

//Qt
#include <QtSql>
#include <QList>

class DBManager
{
public:
    DBManager(const QString &driver, const QString &host, int port, const QString &dbName, const QString &userName, const QString &password);

    QSqlQuery requestHandle(const QString& query, const QVariantList &arguments);

private:
    QSqlQuery executeQuery(const QString & queryText, const QVariantList& arguments);

    QSqlDatabase m_database;
};
