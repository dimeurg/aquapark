#include "AccountInfoDto.h"

AccountInfoDto::AccountInfoDto(const QString &name, const QString &surname, const QString &email, const QString &phone)
    :name(name), surname(surname), email(email), phone(phone)
{

}

bool AccountInfoDto::isValid()
{
    return (!name.isEmpty() && !surname.isEmpty());
}
