#pragma once
#include <QString>

struct AccountInfoDto
{
    AccountInfoDto(const QString& name, const QString& surname, const QString& email, const QString& phone);

    QString name;
    QString surname;
    QString email;
    QString phone;

    bool isValid();

};
