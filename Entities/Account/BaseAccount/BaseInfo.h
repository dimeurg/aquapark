#pragma once
#include "Contact.h"

struct BaseInfo
{
    BaseInfo(const FullName& fullName, const Contact& contact);

    FullName m_fullName;
    Contact m_contact;

public:
};
