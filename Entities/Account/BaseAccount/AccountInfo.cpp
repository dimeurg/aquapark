#include "AccountInfo.h"

AccountInfo::AccountInfo(const QString &name, const QString &surname, const QString &email, const QString &phone)
    :m_name(name), m_surname(surname), m_email(email), m_phone(phone)
{

}

bool AccountInfo::isValid()
{
    return (!m_name.isEmpty() && !m_surname.isEmpty());
}
