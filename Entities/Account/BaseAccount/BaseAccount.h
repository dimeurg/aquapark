#pragma once
#include "AccountInfo.h"
#include "RequestManager/RequestManagerBase.h"

#include <QObject>

class BaseAccount : public QObject
{
    Q_OBJECT
public:
    BaseAccount(const QString& login, const QString& password, const AccountInfo& accountInfo, QObject* parent = nullptr);
    virtual ~BaseAccount();

    QString login() const;
    void setLogin(const QString &login);

    QString password() const;
    void setPassword(const QString &password);

    AccountInfo accountInfo() const;
    void setAccountInfo(const AccountInfo &accountInfo);

protected:
    QString m_login;
    QString m_password;
    AccountInfo m_accountInfo;
};
