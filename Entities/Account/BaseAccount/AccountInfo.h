#pragma once
#include <QString>

struct AccountInfo
{
    AccountInfo(const QString& name, const QString& surname, const QString& email, const QString& phone);

    QString m_name;
    QString m_surname;
    QString m_email;
    QString m_phone;

    bool isValid();

};
