#pragma once
#include <QString>

struct Contact
{
    Contact(const QString& phone, const QString& email);
    QString m_phone;
    QString m_email;
};

struct FullName
{
    FullName(const QString& name, const QString& surname);
    QString m_name;
    QString m_surname;
};
