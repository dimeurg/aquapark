#include "AccountDto.h"

AccountDto::AccountDto(const QString &login, const QString &password, const AccountInfoDto &accountInfo, QObject *parent)
    : QObject(parent),
      m_login(login), m_password(password), m_accountInfo(accountInfo)
{

}

AccountDto::~AccountDto()
{

}

QString AccountDto::login() const
{
    return m_login;
}

void AccountDto::setLogin(const QString &login)
{
    m_login = login;
}

QString AccountDto::password() const
{
    return m_password;
}

void AccountDto::setPassword(const QString &password)
{
    m_password = password;
}

AccountInfoDto AccountDto::accountInfo() const
{
    return m_accountInfo;
}

void AccountDto::setAccountInfo(const AccountInfoDto &accountInfo)
{
    m_accountInfo = accountInfo;
}
