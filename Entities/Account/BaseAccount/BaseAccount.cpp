#include "BaseAccount.h"

BaseAccount::BaseAccount(const QString &login, const QString &password, const AccountInfo &accountInfo, QObject *parent)
    : QObject(parent),
      m_login(login), m_password(password), m_accountInfo(accountInfo)
{

}

BaseAccount::~BaseAccount()
{

}

QString BaseAccount::login() const
{
    return m_login;
}

void BaseAccount::setLogin(const QString &login)
{
    m_login = login;
}

QString BaseAccount::password() const
{
    return m_password;
}

void BaseAccount::setPassword(const QString &password)
{
    m_password = password;
}

AccountInfo BaseAccount::accountInfo() const
{
    return m_accountInfo;
}

void BaseAccount::setAccountInfo(const AccountInfo &accountInfo)
{
    m_accountInfo = accountInfo;
}
