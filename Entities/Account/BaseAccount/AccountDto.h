#pragma once
#include "AccountInfoDto.h"
#include "RequestManager/RequestManagerBase.h"

#include <QObject>

class AccountDto : public QObject
{
    Q_OBJECT
public:
    AccountDto(const QString& login, const QString& password, const AccountInfoDto& accountInfo, QObject* parent = nullptr);
    virtual ~AccountDto();

    QString login() const;
    void setLogin(const QString &login);

    QString password() const;
    void setPassword(const QString &password);

    AccountInfoDto accountInfo() const;
    void setAccountInfo(const AccountInfoDto &accountInfo);

protected:
    QString m_login;
    QString m_password;
    AccountInfoDto m_accountInfo;
};
