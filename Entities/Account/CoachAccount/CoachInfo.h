#pragma once

#include "BaseAccount/BaseInfo.h"

class CoachInfo : public BaseInfo
{
public:
    CoachInfo(const QString& name, const QString& surname, const Contact& contact);
};
