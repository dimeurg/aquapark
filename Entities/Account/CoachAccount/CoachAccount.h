#pragma once

//Custom
#include "BaseAccount/BaseAccount.h"
#include "RequestManager/RequestManagerCoach.h"
#include "Schedule/ClassSchedule.h"
#include "CoachInfo.h"

class CoachAccount : public BaseAccount
{
    Q_OBJECT
public:

    CoachAccount(const QString &login, const QString &password, const CoachInfo & accountInfo);

    Q_INVOKABLE ClassSchedule getScheduleBetweenDates(const QDate& beginDate, const QDate& endDate);
    Q_INVOKABLE ClassSchedule getScheduleThisWeek();
    Q_INVOKABLE ClassSchedule getScheduleNextWeek();

private:
    CoachInfo m_accountInfo;
    RequestManagerCoach m_requestManager;
};
