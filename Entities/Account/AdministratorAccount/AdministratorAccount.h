#pragma once

//Qt
#include <QDate>
#include <QMap>

//Custom
#include "BaseAccount/AccountDto.h"
#include "RequestManager/RequestManagerAdministrator.h"
#include "Servises/IAccountService.h"
#include "Servises/IAccountInfoService.h"
#include "Servises/ISeasonTicketsAdministratorService.h"

enum class SeasonTicketType
{
    Blue,
    Yellow,
    Silver,
    Gold
};

const QMap<SeasonTicketType, QString> SeasonTicketString
{
    {SeasonTicketType::Blue, "Blue"},
    {SeasonTicketType::Yellow, "Yellow"},
    {SeasonTicketType::Silver, "Silver"},
    {SeasonTicketType::Gold, "Gold"}
};

class AdministratorAccount : public AccountDto
{
public:
    AdministratorAccount(const QString& login, const QString& password, const QString& name, const QString& surname, const QString &email, const QString &phoneNumber,
                         const DBManager &dbManager, std::shared_ptr<Loger> loger = nullptr);

    AdministratorAccount(const QString& login, const QString& password, const AccountInfoDto& accountInfo, const DBManager &dbManager, std::shared_ptr<Loger> loger = nullptr);

    virtual ~AdministratorAccount();

    bool checkCustomerAccessHall(const QString& name, const QString& surname, HallType hallType);
    void sellSeasonTicket(const QString& name, const QString& surname, const QString& seasonTicketName, const QDate& dateStart, const QDate& dateEnd);
    void createNewCustomer(const AccountInfoDto& accountInfo);

    std::set<SeasonTicketDto> getSeasonTicketsForCustomer(const QString& name, const QString& surname);

    AccountInfoDto getAccountInfo(const QString& name, const QString& surname);

private:
    ISeasonTicketsAdministratorService stService;
    IAccountService accService;
    IAccountInfoService aiService;
};
