#include "AdministratorAccount.h"

AdministratorAccount::AdministratorAccount(const QString &login, const QString &password, const AccountInfoDto &accountInfo, const DBManager &dbManager, std::shared_ptr<Loger> loger)
    : AccountDto(login, password, accountInfo), stService(dbManager, loger), accService(dbManager, loger), aiService(dbManager, loger)
{
    AccountInfoDto info = getAccountInfo(accountInfo.name, accountInfo.surname);
    if(!info.isValid())
    {
        aiService.createNewAccountInfo(AccountType::Administrator, accountInfo);
    }

    accService.createNewAccount(AccountType::Administrator, AccountDto(login, password, info));
}

AdministratorAccount::AdministratorAccount(const QString &login, const QString &password, const QString &name, const QString &surname, const QString &email, const QString &phoneNumber,
                                           const DBManager &dbManager, std::shared_ptr<Loger> loger)

    :AdministratorAccount(login, password, AccountInfoDto(name, surname, email, phoneNumber), dbManager, loger)
{

}

AdministratorAccount::~AdministratorAccount()
{

}

void AdministratorAccount::sellSeasonTicket(const QString &name, const QString &surname, const QString &seasonTicketName, const QDate &dateStart, const QDate &dateEnd)
{
    AccountInfoDto info = getAccountInfo(name, surname);
    SeasonTicketDto ticket = stService.getSeasonTicket(seasonTicketName);
    ticket.setDataStart(dateStart);
    stService.sellSeasonTicket(info, ticket);
}

void AdministratorAccount::createNewCustomer(const AccountInfoDto &accountInfo)
{
    aiService.createNewAccountInfo(AccountType::Customer, accountInfo);
}

std::set<SeasonTicketDto> AdministratorAccount::getSeasonTicketsForCustomer(const QString &name, const QString &surname)
{
    AccountInfoDto info = getAccountInfo(name, surname);
    return stService.getSeasonTickets(info);
}

AccountInfoDto AdministratorAccount::getAccountInfo(const QString &name, const QString &surname)
{
    AccountInfoDto info = aiService.getAccountInfo(name, surname);
    return info;
}
