#include "CustomerAccount.h"

CustomerAccount::CustomerAccount(const QString &login, const QString &password, const AccountInfoDto & accountInfo)
    :AccountDto(login, password, accountInfo), m_accountInfo(accountInfo)
{
    updateSeasonTickets();
}

CustomerAccount::CustomerAccount(const QString &login, const QString &password, const QString &name, const QString &surname, const QString &email, const QString &phoneNumber)
    :CustomerAccount(login, password, AccountInfoDto(name, surname, email, phoneNumber))
{

}

std::set<SeasonTicketDto> CustomerAccount::getSeasonTickets()
{
    return m_seasonTickets;
}

void CustomerAccount::addSeasonTicket(const SeasonTicketDto &ticket)
{
    SeasonTicketDto newTicket = ticket;
    if(m_seasonTickets.empty())
    {
        QDate currentDate = QDate::currentDate();
        newTicket.setDataStart(currentDate);
    }
    else
    {
        newTicket.setDataStart(m_seasonTickets.rbegin()->dataEnd.addDays(1));
    }

    m_seasonTickets.insert(std::move(newTicket));
}

void CustomerAccount::updateSeasonTickets()
{
    //todo
}

AccountInfoDto CustomerAccount::getAccountInfo() const
{
    return m_accountInfo;
}

void CustomerAccount::setAccountInfo(const AccountInfoDto &accountInfo)
{
    m_accountInfo = accountInfo;
}
