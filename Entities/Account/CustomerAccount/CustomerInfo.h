#pragma once

//Custom
#include "BaseAccount/BaseInfo.h"

class CustomerInfo : public BaseInfo
{
public:
    CustomerInfo(const FullName& fullName, const Contact& contact);

    bool isValid();

};
