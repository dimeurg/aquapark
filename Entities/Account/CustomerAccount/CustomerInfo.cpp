#include "CustomerInfo.h"
CustomerInfo::CustomerInfo(const FullName &fullName, const Contact &contact)
    :BaseInfo(fullName, contact)
{

}

bool CustomerInfo::isValid()
{
    if(m_fullName.m_name.isEmpty() || m_fullName.m_surname.isEmpty())
    {
        return false;
    }

    return true;
}
