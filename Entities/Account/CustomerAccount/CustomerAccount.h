#pragma once

//Qt
#include <QDate>

//Custom
#include "BaseAccount/AccountDto.h"
#include "RequestManager/RequestManagerCustomer.h"

class CustomerAccount : public AccountDto
{
public:
    CustomerAccount(const QString &login, const QString &password, const AccountInfoDto & accountInfo);
    CustomerAccount(const QString &login, const QString &password, const QString &name, const QString &surname, const QString &email, const QString &phoneNumber);

    Q_INVOKABLE std::set<SeasonTicketDto> getSeasonTickets();
    Q_INVOKABLE void addSeasonTicket(const SeasonTicketDto& ticket);

    AccountInfoDto getAccountInfo() const;
    void setAccountInfo(const AccountInfoDto &accountInfo);

private:
    void updateSeasonTickets();

    AccountInfoDto m_accountInfo;
    std::set<SeasonTicketDto> m_seasonTickets;
    RequestManagerCustomer m_requestManager;
};
