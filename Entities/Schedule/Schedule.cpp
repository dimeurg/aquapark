#include "Schedule.h"
#include <QDebug>

Schedule::Schedule()
{

}

void Schedule::addScheduleItem(const ScheduleItem &scheduleItem)
{
    auto pos = findSchedule(scheduleItem);
    if(pos == m_scheduleItems.end())
    {
        m_scheduleItems.insert(scheduleItem);
    }

    else
    {
        //message
        qDebug() << "the schedule already exist, maybe you need to change it";
    }
}

std::set<ScheduleItem> &Schedule::getScheduleItems()
{
    return m_scheduleItems;
}

std::set<ScheduleItem>::iterator Schedule::findSchedule(const ScheduleItem &scheduleItem)
{
    auto pos = std::find_if(m_scheduleItems.begin(), m_scheduleItems.end(), [scheduleItem](const ScheduleItem& current)
    {
        if(current.m_idHall == scheduleItem.m_idHall)
        {
            if(current.m_start.day() == scheduleItem.m_start.day())
            {
                return true;
            }
        }
        return false;
    });

    return pos;
}
