#pragma once

//Custom
#include "ScheduleItem/Class.h"

//stl
#include <set>

class ClassSchedule
{
public:
    ClassSchedule();

    void addScheduleItem(const Class& scheduleItem);
    std::set<Class>& getScheduleItems();

private:
    std::set<Class>::iterator findSchedule(const Class& schedule);
    std::set<Class> m_scheduleItems;
};
