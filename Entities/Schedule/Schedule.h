#pragma once
#include "ScheduleItem/ScheduleItem.h"
#include <set>
#include <QPair>

class Schedule
{
public:
    Schedule();

    void addScheduleItem(const ScheduleItem& scheduleItem);
    std::set<ScheduleItem>& getScheduleItems();

private:
    std::set<ScheduleItem>::iterator findSchedule(const ScheduleItem& schedule);
    std::set<ScheduleItem> m_scheduleItems;
};
