#include "SeasonTicket.h"

SeasonTicket::SeasonTicket(const QString& name, int durationDays, float price, const QString &hallName, QDate dateStart, QDate dateEnd)
    :m_name(name), m_durationDays(durationDays), m_price(price),
     m_hallName(hallName), m_dataStart(dateStart), m_dataEnd(dateEnd)
{

}

bool SeasonTicket::operator<(const SeasonTicket &compareTicket) const
{
    if(m_price < compareTicket.m_price)
    {
        return true;
    }

    else
    {
        if(m_price > compareTicket.m_price)
        {
            return false;
        }

        else
        {
            return m_dataStart < compareTicket.m_dataStart;
        }
    }
}

void SeasonTicket::setDataStart(const QDate& dataStart)
{
    if(!dataStart.isValid())
    {
        return;
    }

    if(m_dataStart != dataStart)
    {
        m_dataStart = dataStart;
        m_dataEnd = m_dataStart.addDays(m_durationDays);
    }
}

bool SeasonTicket::isExist(const QString &name)
{
    //to do
    return true;
}
