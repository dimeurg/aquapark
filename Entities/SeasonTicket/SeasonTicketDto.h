#pragma once
#include <QDate>

struct SeasonTicketDto
{
    SeasonTicketDto(const QString& name, int durationDays, float price, const QString& hallName, QDate dateStart = QDate(), QDate dateEnd = QDate());

    bool operator<(const SeasonTicketDto& compareTicket) const;

    void setDataStart(const QDate& dataStart);
    static bool isExist(const QString& name);

    QString name;
    int durationDays;
    float price;
    QString hallName;

    QDate dataStart;
    QDate dataEnd;
};
