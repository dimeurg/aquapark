#pragma once
#include <QDate>

struct SeasonTicket
{
    SeasonTicket(const QString& name, int durationDays, float price, const QString& hallName, QDate dateStart = QDate(), QDate dateEnd = QDate());

    bool operator<(const SeasonTicket& compareTicket) const;

    void setDataStart(const QDate& dataStart);
    static bool isExist(const QString& name);

    QString m_name;
    int m_durationDays;
    float m_price;
    QString m_hallName;

    QDate m_dataStart;
    QDate m_dataEnd;
};
