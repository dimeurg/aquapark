#include "SeasonTicketDto.h"

SeasonTicketDto::SeasonTicketDto(const QString& name, int durationDays, float price, const QString &hallName, QDate dateStart, QDate dateEnd)
    :name(name), durationDays(durationDays), price(price),
     hallName(hallName), dataStart(dateStart), dataEnd(dateEnd)
{

}

bool SeasonTicketDto::operator<(const SeasonTicketDto &compareTicket) const
{
    if(price < compareTicket.price)
    {
        return true;
    }

    else
    {
        if(price > compareTicket.price)
        {
            return false;
        }

        else
        {
            return dataStart < compareTicket.dataStart;
        }
    }
}

void SeasonTicketDto::setDataStart(const QDate& dataStart_)
{
    if(!dataStart_.isValid())
    {
        return;
    }

    if(dataStart != dataStart_)
    {
        dataStart = dataStart_;
        dataEnd = dataStart.addDays(durationDays);
    }
}

bool SeasonTicketDto::isExist(const QString &name)
{
    //to do
    return true;
}
