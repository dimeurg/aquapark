#pragma once

//stl
#include <set>

//Custom
#include "JsonParser/JsonParser.h"
#include "RequestManagerBase.h"
#include "SeasonTicket/SeasonTicketDto.h"
#include "BaseAccount/AccountDto.h"


class RequestManagerCustomer : public RequestManagerBase
{
public:
    RequestManagerCustomer(const DBManager& dbmanager = DBManager("QMYSQL", "127.0.0.1", 3307, "aquapark", "root", "root"));
    virtual ~RequestManagerCustomer();
};
