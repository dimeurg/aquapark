#pragma once
//Qt
#include <QJsonObject>
#include <QSqlQuery>

class RequestEncoder
{
public:
    static QJsonObject encodeSeasonTicket(const QSqlQuery &responseRow);
    static QJsonObject encodeAccountInfo(const QSqlQuery &responseRow);

};
