#include "RequestEncoder.h"
#include <QtSql>

QJsonObject RequestEncoder::encodeSeasonTicket(const QSqlQuery &responseRow)
{
    QJsonObject jsonTicket;

    jsonTicket.insert("name", responseRow.value("name").toString());
    jsonTicket.insert("duration", responseRow.value("duration").toInt());
    jsonTicket.insert("price", responseRow.value("price").toFloat());
    jsonTicket.insert("hallName", responseRow.value("hallName").toString());
    jsonTicket.insert("dateStart", responseRow.value("dateStart").toDate().toString());
    jsonTicket.insert("dateEnd", responseRow.value("dateEnd").toDate().toString());

    return jsonTicket;
}

QJsonObject RequestEncoder::encodeAccountInfo(const QSqlQuery &responseRow)
{
    QJsonObject jsonCustomer;

    jsonCustomer.insert("name", responseRow.value("name").toString());
    jsonCustomer.insert("surname", responseRow.value("surname").toString());
    jsonCustomer.insert("email", responseRow.value("email").toString());
    jsonCustomer.insert("phone", responseRow.value("phone").toString());

    return jsonCustomer;
}
