#pragma once
#include "RequestManagerBase.h"
#include "SeasonTicket/SeasonTicketDto.h"
#include <set>
#include "BaseAccount/AccountDto.h"
#include "CustomerAccount/CustomerAccount.h"

//Custom
#include "JsonParser/JsonParser.h"

enum class HallType
{
    BigHall,
    ClassHall
};

class RequestManagerAdministrator : public RequestManagerBase
{
public:

    RequestManagerAdministrator(const DBManager& dbmanager = DBManager("QMYSQL", "127.0.0.1", 3307, "aquapark", "root", "root"));
    virtual ~RequestManagerAdministrator();

    void createNewCustomer(const AccountInfoDto& accountInfo);
    void createNewAdministrator(const AccountInfoDto &accountInfo);

    QString GetCustomer(const QString& name, const QString& surname);
    bool isTimePeriodValid(const QDate& start, const QDate& end);

private:

};
