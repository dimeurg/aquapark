#include "RequestManagerBase.h"
#include "RequestEncoder.h"
#include "JsonParser/JsonParser.h"
#include "SeasonTicket/SeasonTicketDto.h"

RequestManagerBase::RequestManagerBase(const DBManager& dbmanager)
    :m_dbManager(dbmanager), info("", "", "", "")
{

}

RequestManagerBase::~RequestManagerBase()
{

}

void RequestManagerBase::createNewAccount(AccountType type, const QString &name, const QString &surname, const QString &login, const QString &password)
{
    QSqlQuery response = m_dbManager.requestHandle(queryCreateNewAccount, {AccountTypeToString.at(AccountType(type)), name, surname, login, password});
}

void RequestManagerBase::createNewAccount(int type, const QString &name, const QString &surname, const QString &login, const QString &password)
{
    createNewAccount(AccountType(type), name, surname, login, password);
}

void RequestManagerBase::createNewAccountInfo(AccountType type, const QString &name, const QString &surname, const QString &email, const QString &phone)
{
    QSqlQuery response = m_dbManager.requestHandle(queryCreateNewAccountInfo, {name, surname, email, phone});
}

void RequestManagerBase::createNewAccountInfo(int type, const QString &name, const QString &surname, const QString &email, const QString &phone)
{
    createNewAccountInfo(AccountType::Customer, name, surname, email, phone);
}

void RequestManagerBase::sellSeasonTicket(const QString &name, const QString &surname, const QString &seasonTicketName, const QDate &dateStart, const QDate &dateEnd)
{
    QSqlQuery response = m_dbManager.requestHandle(querySellTicketByFullNameAndTicketName, {name, surname, seasonTicketName, dateStart, dateEnd});
}

void RequestManagerBase::sellSeasonTicket(const QString &seasonTicketName)
{
    QString response = getSeasonTicket(seasonTicketName);
    SeasonTicketDto ticket = JsonParser::parseOneSeasonTicketJson(response);
    ticket.setDataStart(QDate::currentDate());
    sellSeasonTicket(info.name, info.surname, seasonTicketName, ticket.dataStart, ticket.dataEnd);
}

QString RequestManagerBase::getAccountInfo(const QString &name, const QString &surname)
{
    QSqlQuery response = m_dbManager.requestHandle(queryGetAccountInfo, {name, surname});

    //need to do at least one
    response.next();

    QJsonObject jsonResponse = RequestEncoder::encodeAccountInfo(response);

    QJsonObject jsonObj;
    jsonObj.insert("accountInfo", jsonResponse);

    return QJsonDocument(jsonObj).toJson();
}

QString RequestManagerBase::getSeasonTickets(const QString &name, const QString &surname)
{
    QSqlQuery response = m_dbManager.requestHandle(queryGetSeasonTickets, {name, surname});

    QJsonArray seasonTickets;
    while(response.next())
    {
        seasonTickets.push_back(RequestEncoder::encodeSeasonTicket(response));
    }

    QJsonObject jsonObj;
    jsonObj.insert("seasonTickets", seasonTickets);

    return QJsonDocument(jsonObj).toJson();
}

QString RequestManagerBase::getSeasonTickets()
{
    QString response = getSeasonTickets(info.name, info.surname);
    return JsonParser::parseSeasonTicketsJsonToList(response);
}

QString RequestManagerBase::getSeasonTicket(const QString &name)
{
    QSqlQuery response = m_dbManager.requestHandle(queryGetSeasonTicket, {name});

    QJsonArray seasonTickets;
    while(response.next())
    {
        seasonTickets.push_back(RequestEncoder::encodeSeasonTicket(response));
    }

    QJsonObject jsonObj;
    jsonObj.insert("seasonTicket", seasonTickets);

    return QJsonDocument(jsonObj).toJson();
}

bool RequestManagerBase::login(const QString &login, const QString &password)
{
    QSqlQuery response = m_dbManager.requestHandle(queryLogin, {login, password});

    //need to do at least one
    response.next();

    QJsonObject jsonResponse = RequestEncoder::encodeAccountInfo(response);
    QJsonObject jsonObj;
    jsonObj.insert("accountInfo", jsonResponse);

    info = JsonParser::parseOneAccountInfo(QJsonDocument(jsonObj).toJson());
    return info.isValid();
}
