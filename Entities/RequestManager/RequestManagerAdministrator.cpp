#include "RequestManagerAdministrator.h"
#include "RequestEncoder.h"

RequestManagerAdministrator::RequestManagerAdministrator(const DBManager &dbmanager)
    :RequestManagerBase(dbmanager)
{

}

RequestManagerAdministrator::~RequestManagerAdministrator()
{

}

void RequestManagerAdministrator::createNewCustomer(const AccountInfoDto &accountInfo)
{
    createNewAccountInfo(AccountType::Customer, accountInfo.name, accountInfo.surname, accountInfo.email, accountInfo.phone);
}

void RequestManagerAdministrator::createNewAdministrator(const AccountInfoDto &accountInfo)
{
    createNewAccountInfo(AccountType::Administrator, accountInfo.name, accountInfo.surname, accountInfo.email, accountInfo.phone);
}

QString RequestManagerAdministrator::GetCustomer(const QString& name, const QString& surname)
{
    return getAccountInfo(name, surname);
}

bool RequestManagerAdministrator::isTimePeriodValid(const QDate &start, const QDate &end)
{
    QDate currentDate = QDate::currentDate();

    return (currentDate >= start) && (currentDate <= end);
}
