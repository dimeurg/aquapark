#pragma once

//stl
#include <map>

//Qt
#include <QJsonObject>
#include <QVariantList>

//Custom
#include "DBManager/DBManagerBase.h"
#include "Account/BaseAccount/AccountDto.h"

enum class AccountType{
    Administrator,
    Customer,
    Coach
};

const QString querySellTicketByFullNameAndTicketName =
    "Insert into customerseasontickets(`account`, type, dateStart, dateEnd)"
    "Values("
    "(Select i.id from accountinfo i where i.name = ? and i.surname = ?),"
    "(Select t.id from seasontickettype t where t.name = ? ),"
    "?, ?)";


const std::map<AccountType, QString> AccountTypeToString{
    {AccountType::Administrator, "administrator"},
    {AccountType::Customer, "customer"},
    {AccountType::Coach, "coach"}
};

const QString queryCreateNewAccount =  "Insert into account(type, info, login, password)                "
                                  "Values((select id from accountType where name = ?),                  "
                                  "(select id from accountinfo i where i.name = ? and i.surname = ?),   "
                                  "?, ?)                                                                ";

const QString queryCreateNewAccountInfo = "Insert into accountinfo(name, surname, email, phone)"
                                          "Values(?, ?, ?, ?)";

const QString queryGetAccountInfo = "Select i.name, i.surname, i.email, i.phone "
                                    "from accountinfo i                         "
                                    "where i.`name` = ? and i.`surname` = ?     ";

const QString queryLogin = "Select i.name, i.surname, i.email, i.phone  "
                           "from accountinfo i, account a               "
                           "where a.info = i.id                         "
                           "and a.`login` = ? and a.`password` = ?      ";

const QString queryGetSeasonTickets = "Select t.name, t.duration, t.price, h.name as 'hallName', c.dateStart, c.dateEnd "
                                      "From seasontickettype t, hall h, customerseasontickets c, accountInfo i          "
                                      "where t.hall = h.id and c.type = t.id and i.id = c.account                       "
                                      "and i.name = ? and i.surname = ?                                                 ";

const QString queryGetSeasonTicket = "Select distinct t.name, t.duration, t.price, h.name as 'hallName'     "
                                      "From seasontickettype t, hall h                                      "
                                      "where t.hall = h.id and t.name = ?                                   ";

class RequestManagerBase : public QObject
{
    Q_OBJECT
public:
    RequestManagerBase(const DBManager& dbmanager = DBManager("QMYSQL", "localhost", 3307, "aquapark", "root", "root"));
    virtual ~RequestManagerBase();

    void createNewAccount(AccountType type, const QString &name, const QString &surname, const QString &login, const QString &password);
    void createNewAccountInfo(AccountType type, const QString& name, const QString& surname, const QString& email, const QString& phone);

    Q_INVOKABLE void createNewAccount(int type, const QString &name, const QString &surname, const QString &login, const QString &password);
    Q_INVOKABLE void createNewAccountInfo(int type, const QString& name, const QString& surname, const QString& email, const QString& phone);

    Q_INVOKABLE void sellSeasonTicket(const QString& name, const QString& surname, const QString& seasonTicketName, const QDate& dateStart, const QDate& dateEnd);
    Q_INVOKABLE void sellSeasonTicket(const QString& seasonTicketName);
    Q_INVOKABLE QString getAccountInfo(const QString& name, const QString& surname);
    Q_INVOKABLE QString getSeasonTickets(const QString& name, const QString& surname);
    Q_INVOKABLE QString getSeasonTickets();
    Q_INVOKABLE QString getSeasonTicket(const QString &name);

    Q_INVOKABLE bool login(const QString &login, const QString &password);

protected:
    DBManager m_dbManager;
    AccountInfoDto info;
};
