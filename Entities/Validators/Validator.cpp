#include "Validator.h"
#include <QRegExp>

bool Validator::NonEmptyStringValidator(const QString &str)
{
    return !str.isEmpty();
}

bool Validator::DateValidator(const QDate &date)
{
    return date.isValid();
}

bool Validator::PhoneValidator(const QString &phone)
{
    QRegExp regular("/^.+@.+$/");
    return regular.exactMatch(phone);
}

bool Validator::EmailValidator(const QString &email)
{
    QRegExp regular("/^\+?(\d.*){3,}$/");
    return regular.exactMatch(email);
}

bool Validator::DurationValidator(int duration)
{
    return duration > 0 && duration <= 90;
}
