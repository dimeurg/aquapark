#pragma once
#include <QString>
#include <QDate>

class Validator{
public:

    static bool NonEmptyStringValidator(const QString& str);
    static bool DateValidator(const QDate& date);
    static bool PhoneValidator(const QString& phone);
    static bool EmailValidator(const QString& email);
    static bool DurationValidator(int duration);
};
