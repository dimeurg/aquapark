#pragma once
#include <exception>
#include <QString>

class CustomException : public std::logic_error{
public:
    CustomException(const QString& text);
    virtual ~CustomException();
};

class AccountCreateException: public CustomException{
    public:
    AccountCreateException(const QString& text):CustomException(text){};
    virtual ~AccountCreateException(){};
};

class AccountInfoCreateException: public CustomException{
    public:
    AccountInfoCreateException(const QString& text):CustomException(text){};
    virtual ~AccountInfoCreateException(){};
};

class LoginException: public CustomException{
    public:
    LoginException(const QString& text):CustomException(text){};
    virtual ~LoginException(){};
};

class ScheduleException: public CustomException{
    public:
    ScheduleException(const QString& text):CustomException(text){};
    virtual ~ScheduleException(){};
};

class SeasonTicketChangeException: public CustomException{
    public:
    SeasonTicketChangeException(const QString& text):CustomException(text){};
    virtual ~SeasonTicketChangeException(){};
};

class SellSeasonTicketException: public CustomException{
    public:
    SellSeasonTicketException(const QString& text):CustomException(text){};
    virtual ~SellSeasonTicketException(){};
};

class ServiceValidationException: public CustomException{
    public:
    ServiceValidationException(const QString& text):CustomException(text){};
    virtual ~ServiceValidationException(){};
};

class ServiceUnresolvedEntityException: public CustomException{
    public:
    ServiceUnresolvedEntityException(const QString& text):CustomException(text){};
    virtual ~ServiceUnresolvedEntityException(){};
};

class ApplicationFatalException: public CustomException{
    public:
    ApplicationFatalException(const QString& text):CustomException(text){};
    virtual ~ApplicationFatalException(){};
};

