#include "customException.h"
CustomException::CustomException(const QString &text)
    : std::logic_error(text.toStdString().c_str())
{

}

CustomException::~CustomException()
{

}
