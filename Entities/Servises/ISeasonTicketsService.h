#pragma once
#include "RequestManager/RequestManagerCustomer.h"
#include "SeasonTicket/SeasonTicketDto.h"
#include "set"
#include "IBaseService.h"

class ISeasonTicketsService : public IBaseService
{
public:
    ISeasonTicketsService(const DBManager& dbManager, std::shared_ptr<Loger> loger = nullptr);
    void sellSeasonTicket(const AccountInfoDto& accountInfo, const SeasonTicketDto& ticket);
    std::set<SeasonTicketDto> getSeasonTickets(const AccountInfoDto& accountInfo);
    SeasonTicketDto getSeasonTicket(const QString& name);

private:
    RequestManagerCustomer requestManager;
};

