#include "IAccountInfoService.h"
#include "JsonParser/JsonParser.h"

IAccountInfoService::IAccountInfoService(const DBManager &dbmanager, std::shared_ptr<Loger> loger)
    :requestManager(dbmanager), IBaseService(loger)
{

}

void IAccountInfoService::createNewAccountInfo(AccountType type, AccountInfoDto info)
{
    if(Validator::NonEmptyStringValidator(info.name) && Validator::NonEmptyStringValidator(info.surname))
    {
        requestManager.createNewAccountInfo(type, info.name, info.surname, info.email, info.phone);
        loger->log("Valid create new acount info");
    }
    else
    {
        loger->log("Bad arguments for create new acount info");
    }
}

AccountInfoDto IAccountInfoService::getAccountInfo(const QString &name, const QString &surname)
{
    if(Validator::NonEmptyStringValidator(name) && Validator::NonEmptyStringValidator(surname))
    {
        return JsonParser::parseOneAccountInfo(requestManager.getAccountInfo(name, surname));
        loger->log("Valid get new acount info");
    }
    else
    {
        loger->log("Bad arguments for get acount info");
    }
}
