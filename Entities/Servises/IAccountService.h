#pragma once
#include "RequestManager/RequestManagerCustomer.h"
#include "Account/BaseAccount/AccountLoginDto.h"
#include "IBaseService.h"

class IAccountService: public IBaseService
{
public:
    IAccountService(const DBManager& dbManager, std::shared_ptr<Loger> loger = nullptr);
    void createNewAccount(AccountType type, const AccountDto& account);
    AccountLoginDto login(const QString& login, const QString& password);
    void logout();



private:
    RequestManagerCustomer requestManager;
};

