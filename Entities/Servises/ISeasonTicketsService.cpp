#include "ISeasonTicketsService.h"
#include "JsonParser/JsonParser.h"

ISeasonTicketsService::ISeasonTicketsService(const DBManager &dbManager, std::shared_ptr<Loger> loger)
    :requestManager(dbManager), IBaseService(loger)
{

}

void ISeasonTicketsService::sellSeasonTicket(const AccountInfoDto &accountInfo, const SeasonTicketDto &ticket)
{
    if(!Validator::NonEmptyStringValidator(ticket.name))
    {
        loger->log("Bad ticket name");
        return;
    }

    if(!Validator::DateValidator(ticket.dataStart) && Validator::DateValidator(ticket.dataEnd))
    {
        loger->log("Bad ticket date");
        return;
    }

    if(!Validator::DurationValidator(ticket.durationDays))
    {
        loger->log("Bad ticket duration");
        return;
    }

    else
    {
        loger->log("Valid sell ticket");
        requestManager.sellSeasonTicket(accountInfo.name, accountInfo.surname, ticket.name, ticket.dataStart, ticket.dataEnd);
    }
}

std::set<SeasonTicketDto> ISeasonTicketsService::getSeasonTickets(const AccountInfoDto &accountInfo)
{
    auto tickets = JsonParser::parseSeasonTicketsJson(requestManager.getSeasonTickets(accountInfo.name, accountInfo.surname));
    return JsonParser::parseSeasonTicketsJson(requestManager.getSeasonTickets(accountInfo.name, accountInfo.surname));
}

SeasonTicketDto ISeasonTicketsService::getSeasonTicket(const QString &name)
{
    if(!Validator::NonEmptyStringValidator(name))
    {
        loger->log("Bad ticket name");
        throw SellSeasonTicketException("bad name");
    }

    else
    {
        loger->log("Valid ticket name");
        auto ticket = JsonParser::parseOneSeasonTicketJson(requestManager.getSeasonTicket(name));
        return JsonParser::parseOneSeasonTicketJson(requestManager.getSeasonTicket(name));
    }
}
