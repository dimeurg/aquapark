#include "IBaseService.h"

Loger::Loger(const QString &fileName)
{
    out.open(fileName.toStdString());
}

void Loger::log(const QString &text)
{
    out << text.toStdString() << std::endl;
}

IBaseService::IBaseService(std::shared_ptr<Loger> loger)
    :loger(loger)
{

}
