#pragma once
#include "RequestManager/RequestManagerBase.h"
#include "IBaseService.h"

class IClassService :IBaseService
{
public:
    IClassService(const DBManager& dbManager, std::shared_ptr<Loger> loger = nullptr);
    void addClass(const ClassDto& Thisclass);
    void changeClass(const ClassDto& oldClass, const ClassDto& newClass);
    ClassDto getClass(const QDateTime& date, const HallDto& hall);
    void addNewParticipant(const ClassDto& thisClass, const AccountInfoDto& info);
    void cancelParticipant(const ClassDto& thisClass, const AccountInfoDto& info);

private:
    RequestManagerBase requestManager;
};
