#pragma once
#include "Validators/Validator.h"
#include "Validators/customException.h"
#include <fstream>
#include <memory>

class Loger
{
public:
    Loger(const QString& fileName);
    void log(const QString& text);

private:
    std::ofstream out;
};

class IBaseService
{
protected:
    IBaseService(std::shared_ptr<Loger> loger = nullptr);
    std::shared_ptr<Loger> loger;
};

