#pragma once
#include "RequestManager/RequestManagerBase.h"
#include "IBaseService.h"

class IScheduleService : publc IBaseService
{
public:
    IScheduleService(const DBManager& dbManager, std::shared_ptr<Loger> loger = nullptr);
    void addSchedule(const ScheduleDto& schedule);
    void changeSchedule(const ScheduleDto& oldSchedule, const ScheduleDto& newSchedule);
    ScheduleDto getScheduleForHall(const QString& name);

private:
    RequestManagerBase requestManager;
};
