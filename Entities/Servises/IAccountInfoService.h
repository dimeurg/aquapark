#pragma once
#include "RequestManager/RequestManagerCustomer.h"
#include "IBaseService.h"

class IAccountInfoService : public IBaseService
{
public:
    IAccountInfoService(const DBManager& dbManager, std::shared_ptr<Loger> loger = nullptr);
    void createNewAccountInfo(AccountType type, AccountInfoDto info);
    AccountInfoDto getAccountInfo(const QString& name, const QString& surname);

private:
    RequestManagerCustomer requestManager;
};

