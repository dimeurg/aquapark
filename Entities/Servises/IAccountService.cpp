#include "IAccountService.h"

IAccountService::IAccountService(const DBManager &dbmanager, std::shared_ptr<Loger> loger)
    :requestManager(dbmanager), IBaseService(loger)
{

}

void IAccountService::createNewAccount(AccountType type, const AccountDto &account)
{
    if(Validator::NonEmptyStringValidator(account.login()) && Validator::NonEmptyStringValidator(account.password()))
    {
        requestManager.createNewAccountInfo(type, account.accountInfo().name, account.accountInfo().surname, account.accountInfo().email, account.accountInfo().phone);
        loger->log("Valid  create new acount");
    }
    else
    {
        loger->log("Bad arguments for create new acount");
    }
    requestManager.createNewAccount(type, account.accountInfo().name, account.accountInfo().surname, account.login(), account.password());
}

AccountLoginDto IAccountService::login(const QString &login, const QString &password)
{
    if(Validator::NonEmptyStringValidator(login) && Validator::NonEmptyStringValidator(password))
    {
        //todo
        loger->log("Valid login");
    }
    else
    {
        loger->log("Bad arguments for login");
    }
}

void IAccountService::logout()
{
    //todo
}
