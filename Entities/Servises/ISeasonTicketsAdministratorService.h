#pragma once
#include "ISeasonTicketsService.h"
#include "RequestManager/RequestManagerAdministrator.h"

class ISeasonTicketsAdministratorService : public ISeasonTicketsService
{
public:
    ISeasonTicketsAdministratorService(const DBManager& dbManager, std::shared_ptr<Loger> loger = nullptr);
    void addSeasonTicket(const SeasonTicketDto& ticket);
    void changeSeasonTicket(const SeasonTicketDto& oldTicket, const SeasonTicketDto& newTicket);

private:
    RequestManagerAdministrator requestManager;
};
