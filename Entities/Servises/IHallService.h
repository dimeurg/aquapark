#pragma once
#include "RequestManager/RequestManagerBase.h"
#include "IBaseService.h"

class IHallService :public IBaseService
{
public:
    IHallService(const DBManager& dbManager, std::shared_ptr<Loger> loger = nullptr);
    void addHallInfo(const HallDto& hall);
    void changeHallInfo(const HallDto& hall);
    HallDto getHallInfo(const QString& name);

private:
    RequestManagerAdministrator requestManager;
};
