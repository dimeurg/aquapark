#pragma once

//Qt
#include <QDate>

struct ScheduleItem
{
    ScheduleItem(const QDate& start, const QDate& end, int idHall);
    QDate duration();
    QDate m_start;
    QDate m_end;
    int m_idHall;
};
