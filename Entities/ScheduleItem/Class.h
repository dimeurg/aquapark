#pragma once

//Custom
#include "ScheduleItem.h"
#include "CoachAccount/CoachAccount.h"
#include "CustomerAccount/CustomerInfo.h"

struct Class : public ScheduleItem
{
    Class(const QDate& start, const QDate& end, int idHall, const CoachInfo& couchInfo);

    CoachInfo m_couchInfo;
    std::vector<CustomerInfo> m_customers;
};
