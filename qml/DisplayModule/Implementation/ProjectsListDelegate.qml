import QtQuick 2.0
import DisplayModule.Base 1.0
import StyleSettings 1.0
import QtQuick.Controls 2.12


Rectangle {
    id: root
    color: Style.backgroundColor


    Button {
        id: _logButton

        anchors.top: _password.bottom
        anchors.topMargin: Style.mediumOffset
        anchors.left: _password.left

        text: "Вход"

        onClicked: {
            if(global.login(_email.text, _password.text))
            {
                root.login()
                _email.text = ""
                _password.text = ""
            }
        }
    }

    BaseText {
        id: _projectName
        anchors.left: _icon.right
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width / 3
        height: parent.height / 3

        horizontalAlignment: TextInput.AlignHCenter
        text: projectName
    }
}


