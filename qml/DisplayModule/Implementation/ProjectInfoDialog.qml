import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.12
import DisplayModule.Base 1.0
import StyleSettings 1.0

Dialog {
    id:root

    implicitWidth: 700
    implicitHeight: 600

    standardButtons: StandardButton.Cancel | StandardButton.Ok

    TextField {
        id: _tiket

        anchors.top: parent.top
        anchors.topMargin: Style.mediumOffset
        anchors.left: parent.left
        width: parent.width / 3
        height: 50

        placeholderText: "Название абонемента"
        font.pointSize: 13
        color: Style.backgroundColor

        background: Rectangle {
            implicitWidth: parent.width
            implicitHeight: 30
            color: Style.basicColor
            border.color: Style.basicColor
        }
    }

        Button {
            id: _changeNameButton
            anchors.left: _tiket.right
            anchors.top: _tiket.top
            anchors.leftMargin: Style.mediumOffset
            height:_tiket.height

            text: "Купить"

            onClicked: {
                global.sellSeasonTicket(_tiket.text)
            }
        }


}


