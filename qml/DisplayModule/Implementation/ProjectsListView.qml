import QtQuick 2.0
import DisplayModule.Base 1.0
import StyleSettings 1.0
import QtQuick.Controls 2.12


Rectangle {
    id: root
    color: Style.backgroundColor
    property alias name: _projectName

    ProjectInfoDialog {
        id: _infoDialog

        onClosed: {
            _projectName.text = global.getSeasonTickets()
        }
    }

    Button {
        id: _logButton

        anchors.top: parent.top
        anchors.topMargin: Style.mediumOffset
        anchors.left: parent.left

        text: "Купить абонемент"

        onClicked: {
             _infoDialog.open()
        }
    }

    BaseText {
        id: _projectName
        anchors.top: _logButton.bottom
        anchors.left: parent.left
        width: parent.width/2
        height: parent.height/3
        horizontalAlignment: TextInput.AlignLeft
        text: ""
    }


}

