import QtQuick 2.0
import StyleSettings 1.0
import DisplayModule.Base 1.0
import QtQuick.Controls 2.12

Rectangle {
    id:root
    color: Style.backgroundColor

    BaseText {
        id: _loginLable
        anchors.top: parent.top
        anchors.topMargin: Style.basicMargin
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width / 8
        height: parent.height / 14
        text: "Registration"
    }

    TextField {
        id: _login

        anchors.top: _loginLable.bottom
        anchors.topMargin: Style.mediumOffset
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width / 3
        height: _loginLable.height

        placeholderText: "Email"
        font.pointSize: 13
        color: Style.backgroundColor

        background: Rectangle {
            implicitWidth: parent.width
            implicitHeight: 30
            color: Style.basicColor
            border.color: Style.basicColor
        }
    }

    TextField {
        id: _password

        echoMode: TextField.Password

        anchors.top: _login.bottom
        anchors.topMargin: Style.mediumOffset
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width / 3
        height: _loginLable.height

        placeholderText: "Password"

        font.pointSize: 13
        color: Style.backgroundColor

        background: Rectangle {
            implicitWidth: parent.width
            implicitHeight: 30
            color: Style.basicColor
            border.color: Style.basicColor
        }
    }

    TextField {
        id: _name

        anchors.top: _password.bottom
        anchors.topMargin: Style.mediumOffset
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width / 3
        height: _loginLable.height

        placeholderText: "Name"

        font.pointSize: 13
        color: Style.backgroundColor

        background: Rectangle {
            implicitWidth: parent.width
            implicitHeight: 30
            color: Style.basicColor
            border.color: Style.basicColor
        }
    }

    TextField {
        id: _surname

        anchors.top: _name.bottom
        anchors.topMargin: Style.mediumOffset
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width / 3
        height: _loginLable.height

        placeholderText: "Surname"

        font.pointSize: 13
        color: Style.backgroundColor

        background: Rectangle {
            implicitWidth: parent.width
            implicitHeight: 30
            color: Style.basicColor
            border.color: Style.basicColor
        }
    }

    TextField {
        id: _phone
        anchors.top: _surname.bottom
        anchors.topMargin: Style.mediumOffset
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width / 3
        height: _loginLable.height

        placeholderText: "Phone"

        font.pointSize: 13
        color: Style.backgroundColor

        background: Rectangle {
            implicitWidth: parent.width
            implicitHeight: 30
            color: Style.basicColor
            border.color: Style.basicColor
        }
    }

    TextField {
        id: _email

        anchors.top: _phone.bottom
        anchors.topMargin: Style.mediumOffset
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width / 3
        height: _loginLable.height

        placeholderText: "Email"

        font.pointSize: 13
        color: Style.backgroundColor

        background: Rectangle {
            implicitWidth: parent.width
            implicitHeight: 30
            color: Style.basicColor
            border.color: Style.basicColor
        }
    }

    Button {
        id: _createButton

        anchors.top: _email.bottom
        anchors.topMargin: Style.mediumOffset
        anchors.horizontalCenter: parent.horizontalCenter
        text: "Зарегистрировать"

        onClicked: {
           global.createNewAccountInfo(1, _name.text, _surname.text, _email.text, _phone.text);
           global.createNewAccount(1, _name.text, _surname.text, _login.text, _password.text);
        }
    }
}
