QT += sql quick

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        Entities/Account/AdministratorAccount/AdministratorAccount.cpp \
        Entities/Account/BaseAccount/AccountDto.cpp \
        Entities/Account/BaseAccount/AccountInfoDto.cpp \
        #Entities/Account/CoachAccount/CoachAccount.cpp \
        #Entities/Account/CoachAccount/CoachInfo.cpp \
        Entities/Account/BaseAccount/AccountLoginDto.cpp \
        Entities/Account/CustomerAccount/CustomerAccount.cpp \
        #Entities/DBManager/DBManagerAdministrator.cpp \
        Entities/DBManager/DBManagerBase.cpp \
        #Entities/DBManager/DBManagerCoach.cpp \
        #Entities/DBManager/DBManagerCustomer.cpp \
        #Entities/Dto/ClassDto.cpp \
        #Entities/Dto/HallDto.cpp \
        #Entities/Dto/ScheduleDto.cpp \
        Entities/JsonParser/JsonParser.cpp \
        Entities/RequestManager/RequestEncoder.cpp \
        Entities/RequestManager/RequestManagerAdministrator.cpp \
        Entities/RequestManager/RequestManagerBase.cpp \
        #Entities/RequestManager/RequestManagerCoach.cpp \
        Entities/RequestManager/RequestManagerCustomer.cpp \
        #Entities/Schedule/ClassSchedule.cpp \
        #Entities/Schedule/Schedule.cpp \
        #Entities/ScheduleItem/Class.cpp \
        #Entities/ScheduleItem/ScheduleItem.cpp \
        Entities/SeasonTicket/SeasonTicketDto.cpp \
        Entities/Servises/IAccountInfoService.cpp \
        Entities/Servises/IAccountService.cpp \
        Entities/Servises/IBaseService.cpp \
        Entities/Servises/IClassService.cpp \
        Entities/Servises/IHallService.cpp \
        Entities/Servises/IScheduleService.cpp \
        Entities/Servises/ISeasonTicketsAdministratorService.cpp \
        Entities/Servises/ISeasonTicketsService.cpp \
        Entities/Validators/Validator.cpp \
        Entities/Validators/customException.cpp \
        main.cpp

RESOURCES += qml.qrc \
    qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH = $$PWD/qml

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += Entities \
               Entities/Account

HEADERS += \
    Entities/Account/AdministratorAccount/AdministratorAccount.h \
    Entities/Account/BaseAccount/AccountDto.h \
    Entities/Account/BaseAccount/AccountInfoDto.h \
    #Entities/Account/CoachAccount/CoachAccount.h \
    #Entities/Account/CoachAccount/CoachInfo.h \
    Entities/Account/BaseAccount/AccountLoginDto.h \
    Entities/Account/CustomerAccount/CustomerAccount.h \
    #Entities/DBManager/DBManagerAdministrator.h \
    Entities/DBManager/DBManagerBase.h \
    #Entities/DBManager/DBManagerCoach.h \
    #Entities/DBManager/DBManagerCustomer.h \
    #Entities/Dto/ClassDto.h \
    #Entities/Dto/HallDto.h \
    #Entities/Dto/ScheduleDto.h \
    Entities/JsonParser/JsonParser.h \
    Entities/RequestManager/RequestEncoder.h \
    Entities/RequestManager/RequestManagerAdministrator.h \
    Entities/RequestManager/RequestManagerBase.h \
    #Entities/RequestManager/RequestManagerCoach.h \
    Entities/RequestManager/RequestManagerCustomer.h \
    #Entities/Schedule/ClassSchedule.h \
    #Entities/Schedule/Schedule.h \
    #Entities/ScheduleItem/Class.h \
    #Entities/ScheduleItem/ScheduleItem.h \
    Entities/SeasonTicket/SeasonTicketDto.h \
    Entities/Servises/IAccountInfoService.h \
    Entities/Servises/IAccountService.h \
    Entities/Servises/IBaseService.h \
    Entities/Servises/IClassService.h \
    Entities/Servises/IHallService.h \
    Entities/Servises/IScheduleService.h \
    Entities/Servises/ISeasonTicketsAdministratorService.h \
    Entities/Servises/ISeasonTicketsService.h \
    Entities/Validators/Validator.h \
    Entities/Validators/customException.h

DISTFILES += \
    main.qml \
    qml/DisplayModule/Base/BaseListDelegate.qml \
    qml/DisplayModule/Base/BaseListView.qml \
    qml/DisplayModule/Base/BaseText.qml \
    qml/DisplayModule/Base/RoundImage.qml \
    qml/DisplayModule/Base/RoundImageSub.qml \
    qml/DisplayModule/Base/qmldir \
    qml/DisplayModule/Implementation/ProjectInfoDialog.qml \
    qml/DisplayModule/Implementation/ProjectsListDelegate.qml \
    qml/DisplayModule/Implementation/ProjectsListView.qml \
    qml/DisplayModule/Implementation/ProjectsLoginView.qml \
    qml/DisplayModule/Implementation/RegisterView.qml \
    qml/DisplayModule/Implementation/qmldir \
    qml/DisplayModule/qmldir \
    qml/StyleSettings/Style.qml \
    qml/StyleSettings/qmldir
