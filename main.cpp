#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "CustomerAccount/CustomerAccount.h"
#include "AdministratorAccount/AdministratorAccount.h"
#include <QDebug>
#include <windows.h>
#include "Entities/RequestManager/RequestManagerBase.h"
#include <QQmlContext>

void printBaseInfo(const AccountInfoDto& info)
{
    qDebug() << "accountInfo";
    qDebug() << "name: " + info.name;
    qDebug() << "surname: " + info.surname;
    qDebug() << "email: " + info.email;
    qDebug() << "phone: " + info.phone;
}

void test()
{
    DBManager databaseInitial("QMYSQL", "localhost", 3307, "", "root", "root");
    const QString queryInitializeScheme = "CREATE DATABASE `test`;";
    databaseInitial.requestHandle(queryInitializeScheme, {});

    DBManager databaseQuery("QMYSQL", "localhost", 3307, "test", "root", "root");
    const QString createTables = ""
    "      CREATE TABLE `accounttype` (                                      "
    "        `id` int(11) NOT NULL AUTO_INCREMENT,                           "
    "        `name` text,                                                    "
    "        PRIMARY KEY (`id`)                                              "
    "      );                                                                "
    "      CREATE TABLE `accountinfo` (                                      "
    "        `id` int(11) NOT NULL AUTO_INCREMENT,                           "
    "        `name` text NOT NULL,                                           "
    "        `surname` text NOT NULL,                                        "
    "        `email` text,                                                   "
    "        `phone` text,                                                   "
    "        PRIMARY KEY (`id`)                                              "
    "      );                                                                "
    "      CREATE TABLE `location` (                                         "
    "        `id` int(11) NOT NULL AUTO_INCREMENT,                           "
    "        `address` text,                                                 "
    "        `phone` text,                                                   "
    "        PRIMARY KEY (`id`)                                              "
    "      );                                                                "
    "                                                                        "
    "      CREATE TABLE `hall` (                                             "
    "        `id` int(11) NOT NULL AUTO_INCREMENT,                           "
    "        `address` int(11) NOT NULL,                                     "
    "        `name` text,                                                    "
    "        PRIMARY KEY (`id`),                                             "
    "        FOREIGN KEY (`address`) REFERENCES `location` (`id`)            "
    "      );                                                                "
    "                                                                        "
    "        CREATE TABLE `account` (                                        "
    "        `id` int(11) NOT NULL AUTO_INCREMENT,                           "
    "        `type` int(11) NOT NULL,                                        "
    "        `info` int(11) NOT NULL,                                        "
    "        `login` text,                                                   "
    "        `password` text,                                                "
    "        PRIMARY KEY (`id`),                                             "
    "        FOREIGN KEY (`type`) REFERENCES `accounttype` (`id`),           "
    "        FOREIGN KEY (`info`) REFERENCES `accountinfo` (`id`)            "
    "      );                                                                "
    "                                                                        "
    "                                                                        "
    "      CREATE TABLE `schedule` (                                         "
    "        `id` int(11) NOT NULL AUTO_INCREMENT,                           "
    "        `hallId` int(11) NOT NULL,                                      "
    "        `dateStart` date NOT NULL,                                      "
    "        `dateEnt` date NOT NULL,                                        "
    "        PRIMARY KEY (`id`),                                             "
    "        FOREIGN KEY (`hallId`) REFERENCES `hall` (`id`)                 "
    "      );                                                                "
    "                                                                        "
    "                                                                        "
    "      CREATE TABLE `class` (                                            "
    "        `id` int(11) NOT NULL AUTO_INCREMENT,                           "
    "        `schedule` int(11) NOT NULL,                                    "
    "        `coachId` int(11) NOT NULL,                                     "
    "        `maxParticipants` int(11) NOT NULL,                             "
    "        PRIMARY KEY (`id`),                                             "
    "        KEY `schedule` (`schedule`),                                    "
    "        KEY `kk_idx` (`coachId`),                                       "
    "        FOREIGN KEY (`schedule`) REFERENCES `schedule` (`id`),          "
    "        FOREIGN KEY (`coachId`) REFERENCES `accountinfo` (`id`)         "
    "      );                                                                "
    "                                                                        "
    "      CREATE TABLE `classes` (                                          "
    "        `id` int(11) NOT NULL AUTO_INCREMENT,                           "
    "        `class` int(11) NOT NULL,                                       "
    "        `customerId` int(11) NOT NULL,                                  "
    "        PRIMARY KEY (`id`),                                             "
    "        FOREIGN KEY (`class`) REFERENCES `class` (`id`),                "
    "        FOREIGN KEY (`customerId`) REFERENCES `accountinfo` (`id`)      "
    "      );                                                                "
    "                                                                        "
    "      CREATE TABLE `seasontickettype` (                                 "
    "        `id` int(11) NOT NULL AUTO_INCREMENT,                           "
    "        `hall` int(11) NOT NULL,                                        "
    "        `name` text NOT NULL,                                           "
    "        `price` int(11) NOT NULL,                                       "
    "        `duration` text NOT NULL,                                       "
    "        PRIMARY KEY (`id`),                                             "
    "        FOREIGN KEY (`hall`) REFERENCES `hall` (`id`)                   "
    "      );                                                                "
    "      CREATE TABLE `customerseasontickets` (                            "
    "        `id` int(11) NOT NULL AUTO_INCREMENT,                           "
    "        `account` int(11) NOT NULL,                                     "
    "        `type` int(11) NOT NULL,                                        "
    "        `dateStart` date NOT NULL,                                      "
    "        `dateEnd` date NOT NULL,                                        "
    "        PRIMARY KEY (`id`),                                             "
    "        FOREIGN KEY (`account`) REFERENCES `accountinfo` (`id`),        "
    "        FOREIGN KEY (`type`) REFERENCES `seasontickettype` (`id`)       "
    "      ) ;                                                               "
    "";

    databaseQuery.requestHandle(createTables, {});

    const QString createTypes =
        "insert into accounttype(`name`)"
        "values('administrator');"
        ""
        "insert into accounttype(`name`)"
        "values('customer');"
        ""
        "insert into location(address, phone)"
        "values('test', '333');"
        ""
        "insert into hall(address, `name`)"
        "values(1, 'Big Hall');"
        ""
        "insert into seasontickettype(hall, `name`, price, duration)"
        "values(1, 'Blue Ticket', 200, 30);";

     databaseQuery.requestHandle(createTypes, {});


    std::shared_ptr<Loger> loger(new Loger("log.txt"));
    AdministratorAccount admin("admin", "root", "Petya", "Petrov", "admin@gmail.com", "3801224124", databaseQuery, loger);

    qDebug() << "admin account";
    qDebug() << "login: " << admin.login();
    qDebug() << "password: " << admin.password();
    printBaseInfo(admin.accountInfo());
    qDebug() << "";


    AccountInfoDto localCustomer("Vasya", "Pup", "email@nure.ua", "2222");
    admin.createNewCustomer(localCustomer);

    admin.sellSeasonTicket("Vasya", "Pup", "Blue Ticket", QDate(2020, 3, 5), QDate(2020, 2, 6));

    AccountInfoDto customerFromDB = admin.getAccountInfo("Vasya", "Pup");
    auto seasonTickets = admin.getSeasonTicketsForCustomer("Vasya", "Pup");

    Sleep(2000);
    const QString queryUninitializeScheme = "drop database `test`;";
    databaseInitial.requestHandle(queryUninitializeScheme, {});

    qDebug() << "local info : ";
    printBaseInfo(localCustomer);
    qDebug() << "";

    qDebug() << "info from db : ";
    printBaseInfo(customerFromDB);
    qDebug() << "";

    qDebug() << "local ticket: ";
    qDebug() << "Blue Ticket";
    qDebug() << "price: " << 200 ;
    qDebug() << "duration: " << 30 ;
    qDebug() << "dateStart: " << QDate(2020, 3, 5);
    qDebug() << "dateEnd: "  << QDate(2020, 2, 6);
    qDebug() << "";

    qDebug() << "ticket from db: ";
    qDebug() << "name: " << seasonTickets.begin()->name;
    qDebug() << "price: " << seasonTickets.begin()->price ;
    qDebug() << "duration: " << seasonTickets.begin()->durationDays ;
    qDebug() << "dateStart: " << seasonTickets.begin()->dataStart;
    qDebug() << "dateEnd: "  << seasonTickets.begin()->dataEnd;
}

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType<RequestManagerBase>("RequestMy", 1, 0, "RequestMy");

    engine.addImportPath(":/qml");
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    DBManager databaseQuery("QMYSQL", "localhost", 3307, "aquapark", "root", "root");
    RequestManagerBase global(databaseQuery);
    engine.rootContext()->setContextProperty("global", &global);

    //test();
    return app.exec();
}
